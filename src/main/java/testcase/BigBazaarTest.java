package testcase;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.Homepage;
import pages.MutualFundsPage;
import wdMethods.ProjectMethods;

public class BigBazaarTest extends ProjectMethods {

	@BeforeClass
	public void setdata() {
		testCaseName = "TC001_BigBazaar";
		testCaseDescription ="Investments";
		category = "Smoke";
		author= "Sruthi";
		dataSheetName="TC001";
	}

	@Test
	public void test() throws InterruptedException {

		new Homepage()
		.mouseOverInvestments()
		.clickSerachMutualFunds()
		.ageSelect()
		.monthclick()
		.dayclick()
		.clickContinue()
		.verifydate()
		.clickContinue()
		.salary()
		.clickContinue()
		.selectbank()
		.firstName()
		.viewFunds()
		.getlist();
	}
}
