package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Homepage extends ProjectMethods{
	
	public Homepage() {
		PageFactory.initElements(driver, this);
	}

	public MutualFundsPage mouseOverInvestments() {
		
		WebElement investmentClick = locateElement("xpath", "//a[text()='INVESTMENTS']");
		Actions builder = new Actions(driver);
		builder.moveToElement(investmentClick).perform();
		WebElement locateElement = locateElement("xpath", "//a[text()='Mutual Funds']");
		builder.pause(2000).click(locateElement).perform();		
		
     return new MutualFundsPage();

	}

}
