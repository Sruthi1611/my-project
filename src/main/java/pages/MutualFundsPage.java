package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import wdMethods.ProjectMethods;

public class MutualFundsPage extends ProjectMethods {

    WebDriverWait wait = new WebDriverWait(driver, 50);
    
	public MutualFundsPage() {
		PageFactory.initElements(driver, this);
	}

	public MutualFundsPage clickSerachMutualFunds() {

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //a[text()='Search for Mutual Funds']")));
		WebElement clicksearch = locateElement("xpath", " //a[text()='Search for Mutual Funds']");
		click(clicksearch);

		return this;
	}
	public MutualFundsPage ageSelect() {
      
		wait.until(ExpectedConditions.elementToBeClickable(By.className("rangeslider__handle")));
		WebElement eleAge = locateElement("class", "rangeslider__handle");
		click(eleAge);

		return this;
	}

	public MutualFundsPage monthclick() {
		WebElement monthclick = locateElement("xpath", "//a[text()='Jan 2000']");
		click(monthclick);
		return this;
	}

	public MutualFundsPage dayclick() {


		WebElement clickdate = locateElement("xpath", "//div[@class='react-datepicker__week']/following::div[text()='27']");
		click(clickdate);

		return this;



	}
	
	public MutualFundsPage clickContinue() {
		WebElement cont = locateElement("linktext", "Continue");
		click(cont);
		return this;
	}

	public MutualFundsPage verifydate() {
		WebElement verifydob = locateElement("xpath", "(//span[contains(@class,'Calendar_highlight')])[3]");
		String dob = getText(verifydob);
		System.out.println("selected DOB is " +dob );
		return this;

	}
	
	

	public MutualFundsPage salary() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //input[@name='netAnnualIncome']")));
		WebElement salary = locateElement("xpath", "//input[@name='netAnnualIncome']");
		type(salary, "425000");
		return this;
	}

	
	
	public MutualFundsPage selectbank() {
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@class='labelContainer']")));
		WebElement bankacc = locateElement("xpath", "//label[@class='labelContainer']");
		click(bankacc);
		return this;
		
	}
	
	public MutualFundsPage firstName() {
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='firstName']")));
		WebElement fname = locateElement("xpath", "//input[@name='firstName']");
		type(fname, "Sruthi");
		return this;
	}
	
	public InvestInYourCustomizedFundPage viewFunds() throws InterruptedException {
		
		WebElement viewfunds = locateElement("linktext", "View Mutual Funds");
		click(viewfunds);
		return new InvestInYourCustomizedFundPage();
	}



}
