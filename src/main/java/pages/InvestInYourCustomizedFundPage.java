package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class InvestInYourCustomizedFundPage extends ProjectMethods {

	public InvestInYourCustomizedFundPage() throws InterruptedException {
		// TODO Auto-generated constructor stub
		Thread.sleep(3000);
		PageFactory.initElements(driver, this);
	}

	public InvestInYourCustomizedFundPage getlist() {

		List<WebElement> name = driver.findElementsByClassName("js-offer-name");
		for (WebElement eachelement : name) {
			System.out.println("Scheme name: "+getText(eachelement));

			WebElement element = driver.findElementByXPath("//span[contains(text(),'"+eachelement.getText()+"')]/following::span[@class='fui-rupee bb-rupee-xs']/..");
			System.out.println("Scheme amount: "+ element.getText());
			
			
		}
		return this;
	}


}
